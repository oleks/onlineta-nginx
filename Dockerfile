FROM alpine:latest
MAINTAINER oleks <oleks@oleks.info>

RUN apk --update --no-cache add nginx

RUN apk --update --no-cache add go

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
